package sample.products.util;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVReaderUtilTest {

    @Test
    void readAllRecords() throws IOException {

        List<String[]> expectedRecords = Arrays.asList(
                new String[]{"43664", "LEGO #14362905", "24.99", "29.99", "0", "LEGO"},
                new String[]{"18450", "s.Oliver #1574690", "35.99", "35.99", "12", "s.Oliver"});
        List<String[]> records = CSVReaderUtil.readAllRecords(
                getClass().getClassLoader().getResourceAsStream("product_data_test.csv"), ','
        );
        assertNotNull(records);
        assertTrue(records.size()==2);
        assertArrayEquals(expectedRecords.get(0), records.get(0));
        assertArrayEquals(expectedRecords.get(1), records.get(1));
    }
}