package sample.products.helper;

import sample.products.dto.ProductDto;
import sample.products.model.Product;

public class ProductDataHelper {

    private ProductDataHelper() {
    }

    public static ProductDto convertToDto(Product product) {

        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName().getName());
        productDto.setIdentificationNumber(product.getName().getIdentificationNumber());
        productDto.setPrice(product.getPrice().toString());
        productDto.setOldPrice(product.getOldPrice().toString());
        productDto.setStock(product.getStock());
        productDto.setBrand(product.getBrand());
        return productDto;
    }
}
