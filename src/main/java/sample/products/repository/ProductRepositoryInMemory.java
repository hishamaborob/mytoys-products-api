package sample.products.repository;

import sample.products.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductRepositoryInMemory implements ProductRepository {

    // No need for a ConcurrentHashMap for now.
    private Map<Long, Product> products = new HashMap<>();

    @Override
    public Product get(long id) {

        return products.get(id);
    }

    @Override
    public List<Product> getAll() {

        return products.values().stream().collect(Collectors.toList());
    }

    @Override
    public void put(Product product) {

        products.put(product.getId(), product);
    }
}
