package sample.products.helper;

import org.junit.jupiter.api.Test;
import sample.products.dto.ProductDto;
import sample.products.model.Product;
import sample.products.model.entity.ProductName;

import static org.junit.jupiter.api.Assertions.*;

class ProductDataHelperTest {

    @Test
    void convertToDto() {

        Product product = new Product(1, new ProductName("name", "111"),
                "2", "2", 3, "test");
        ProductDto expectedProductDto = new ProductDto();
        expectedProductDto.setId(1);
        expectedProductDto.setName("name");
        expectedProductDto.setIdentificationNumber("111");
        expectedProductDto.setPrice("2");
        expectedProductDto.setOldPrice("2");
        expectedProductDto.setStock(3);
        expectedProductDto.setBrand("test");
        ProductDto productDto = ProductDataHelper.convertToDto(product);
        assertNotNull(productDto);
        assertTrue(productDto.equals(expectedProductDto));
    }
}