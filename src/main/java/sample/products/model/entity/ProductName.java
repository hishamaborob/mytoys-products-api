package sample.products.model.entity;

import static com.google.common.base.Preconditions.checkArgument;

public class ProductName {

    private final String name;
    private final String identificationNumber;

    public ProductName(String name, String identificationNumber) {

        checkArgument(
                name != null && !name.isEmpty(),
                "Product Name cannot be empty");
        checkArgument(
                identificationNumber != null && !identificationNumber.isEmpty(),
                "Product Identification Number cannot be empty");
        this.name = name;
        this.identificationNumber = identificationNumber;
    }

    public String getName() {
        return name;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }
}
