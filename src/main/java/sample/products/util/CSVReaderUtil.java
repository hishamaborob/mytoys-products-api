package sample.products.util;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import javax.imageio.IIOException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.Logger;

public class CSVReaderUtil {

    private static final Logger LOGGER = Logger.getLogger(CSVReaderUtil.class);

    private CSVReaderUtil() {
    }

    private static List<String[]> readAll(
            Reader reader, final char separator) throws IOException {

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(separator)
                .withIgnoreQuotations(true)
                .build();
        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();
        List<String[]> list = csvReader.readAll();
        try {
            csvReader.close();
        } catch (IIOException e) {
            LOGGER.debug(e);
        }
        return list;
    }

    public static List<String[]> readAllRecords(
            final InputStream stream, final char separator) throws IOException {

        Reader reader = new InputStreamReader(stream);
        List<String[]> list = readAll(reader, separator);
        try {
            reader.close();
        } catch (IIOException e) {
            LOGGER.debug(e);
        }
        return list;
    }
}
