package sample.products.api;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Check health and handle unmapped requests errors.
 */
@RestController
@ApiIgnore
public class DefaultController implements ErrorController {

    private static final String PATH = "/error";

    @Override
    public String getErrorPath() {
        return PATH;
    }

    @RequestMapping(value = PATH)
    public ResponseEntity<ApiStatus> error(HttpServletRequest request, HttpServletResponse response) {

        return new ResponseEntity<>(new ApiStatus(HttpStatus.NOT_FOUND,
                "Unexpected error or resource not found"), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/_health")
    public ResponseEntity<ApiStatus> health() {

        return new ResponseEntity<>(new ApiStatus(HttpStatus.OK, "Api is online"), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<ApiStatus> home() {

        return new ResponseEntity<>(new ApiStatus(
                HttpStatus.OK,
                "Api is online, please refer to the Swagger documentation /swagger-ui.html for end points."),
                HttpStatus.OK);
    }
}
