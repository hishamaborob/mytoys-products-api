package sample.products.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sample.products.model.Product;
import sample.products.model.entity.ProductName;
import sample.products.repository.ProductRepository;
import sample.products.repository.ProductRepositoryInMemory;
import sample.products.util.CSVReaderUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class DatabaseConfig {

    private static final Logger LOGGER = Logger.getLogger(DatabaseConfig.class);

    @Value("${data.file}")
    private String dataFile;

    @Bean
    public ProductRepository productRepository() throws IOException {

        ProductRepository productRepository = new ProductRepositoryInMemory();
        List<String[]> records = CSVReaderUtil.readAllRecords(
                getClass().getClassLoader().getResourceAsStream(dataFile), ',');
        for (String[] record : records) {
            try {
                List<String> productName =
                        Arrays.stream(record[1].split("#")).map(String::trim).collect(Collectors.toList());
                productRepository.put(new Product(
                        Long.valueOf(record[0]),
                        new ProductName(productName.get(0), productName.get(1)),
                        record[2], record[3], Integer.valueOf(record[4]), record[5]));
            } catch (Exception e) {
                LOGGER.warn("Faild to parse one record: " + e.getMessage(), e);
            }
        }
        return productRepository;
    }
}
