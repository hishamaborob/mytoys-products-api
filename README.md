## Product API
### REST API for (reading) Products

##### Important Note:
- This API implements only read endpoints.
- No concrete features were required, so I implemented the minimal things, no filtering no limit ...
- I know that in the task description, you need the endpoint to be "/product", 
but I implemented it as "/products" because it's better to have plural, and I can justify that during a review.

##### Overview

- A simple REST API based on Java 8, Spring Boot and Maven.
- Currently, the data source is a CSV file held in resources.
- Data will be loaded into memory at the startup.
- /v1/products currently returns a list of all products.
- /v1/products/{id} returns product by the primary key. I didn't use the identification number or ian for now.
- I modeled the Product based on my understanding of it. I also followed some DDD rules. We have Product model and productName entity.
- I use DTO for projection.
- Unit and Integration test provided. (Not 100% test coverage)
- Swagger UI can be used to test the API.

#### Build and Run


In the parent directory:
```
mvn clean package
```

Run:
```
java -jar target/*.jar
```

Location:
```
http://localhost:8080/swagger-ui.html
```