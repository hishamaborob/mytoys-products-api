package sample.products.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.products.model.Product;
import sample.products.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {

        this.productRepository = productRepository;
    }

    public Optional<Product> getProductById(long id) {

        Product product = productRepository.get(id);
        return Optional.ofNullable(product);
    }

    public List<Product> getAllProducts() {

        List<Product> productList = new ArrayList<>();
        productList.addAll(productRepository.getAll());
        return productList;
    }

}
