package sample.products.api;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
class ProductControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getProducts() throws Exception {

        this.mockMvc.perform(get("/v1/products")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    void getProductById() throws Exception {

        this.mockMvc.perform(get("/v1/products/43664")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").exists())
                .andExpect(jsonPath("id").value(43664))
                .andExpect(jsonPath("name").exists())
                .andExpect(jsonPath("name").value("LEGO"))
                .andExpect(jsonPath("identificationNumber").exists())
                .andExpect(jsonPath("identificationNumber").value("14362905"))
                .andExpect(jsonPath("price").exists())
                .andExpect(jsonPath("price").value("24.99"))
                .andExpect(jsonPath("oldPrice").exists())
                .andExpect(jsonPath("oldPrice").value("29.99"))
                .andExpect(jsonPath("stock").exists())
                .andExpect(jsonPath("stock").value(0))
                .andExpect(jsonPath("brand").exists())
                .andExpect(jsonPath("brand").value("LEGO"));
    }

    @Test
    void getProductByIdNotFound() throws Exception {

        this.mockMvc.perform(get("/v1/products/11111111")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isNotFound());
    }
}