package sample.products.api;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sample.products.dto.ProductDto;
import sample.products.helper.ProductDataHelper;
import sample.products.model.Product;
import sample.products.service.ProductService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static sample.products.helper.ProductDataHelper.convertToDto;


@RestController
@RequestMapping("/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of products")
    })
    public List<ProductDto> getProducts() {

        return productService.getAllProducts().stream()
                .map(ProductDataHelper::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product"),
            @ApiResponse(code = 404, message = "Product not found"),
            @ApiResponse(code = 400, message = "Invalid ID")
    })
    public ResponseEntity<ProductDto> getProductById(@PathVariable("id") Long id) {

        Optional<Product> productOptional = productService.getProductById(id);

        if (!productOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
        }
        return new ResponseEntity<>(convertToDto(productOptional.get()), HttpStatus.OK);
    }
}
