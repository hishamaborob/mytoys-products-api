package sample.products.model;

import sample.products.model.entity.ProductName;

import java.math.BigDecimal;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;

public class Product {

    private final long id;
    private final ProductName name;
    private final BigDecimal price;
    private final BigDecimal oldPrice;
    private final int stock;
    private final String brand;

    public Product(long id, ProductName name, String price, String oldPrice, int stock, String brand) {

        checkArgument(
                brand != null && !brand.isEmpty(),
                "Product brand cannot be empty");
        this.id = id;
        this.name = name;
        this.price = new BigDecimal(price);
        this.oldPrice = new BigDecimal(oldPrice);
        this.stock = stock;
        this.brand = brand;
    }

    public long getId() {
        return id;
    }

    public ProductName getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getOldPrice() {
        return oldPrice;
    }

    public int getStock() {
        return stock;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                stock == product.stock &&
                Objects.equals(name, product.name) &&
                Objects.equals(price, product.price) &&
                Objects.equals(oldPrice, product.oldPrice) &&
                Objects.equals(brand, product.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, oldPrice, stock, brand);
    }
}
