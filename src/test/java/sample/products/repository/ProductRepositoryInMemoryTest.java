package sample.products.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sample.products.model.Product;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductRepositoryInMemoryTest {

    private ProductRepository productRepository;

    @BeforeEach
    void setUp() {

        productRepository = new ProductRepositoryInMemory();
        productRepository.put(new Product(1, null, "2", "2", 3, "test"));
        productRepository.put(new Product(2, null, "2", "2", 3, "test"));
    }

    @Test
    void get() {

        Product product1 = productRepository.get(1);
        assertNotNull(product1);
        Product product2 = productRepository.get(2);
        assertNotNull(product2);
    }

    @Test
    void getAll() {

        List<Product> productList = productRepository.getAll();
        assertNotNull(productList);
        assertTrue(productList.size() == 2);
    }
}