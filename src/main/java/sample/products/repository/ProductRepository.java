package sample.products.repository;

import sample.products.model.Product;

import java.util.List;

public interface ProductRepository {

    Product get(long id);

    List<Product> getAll();

    void put(Product product);
}
